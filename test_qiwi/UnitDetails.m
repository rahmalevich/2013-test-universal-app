//
//  UnitDetails.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "UnitDetails.h"
#import "Unit.h"


@implementation UnitDetails

@dynamic to_name_f;
@dynamic cre;
@dynamic kbk;
@dynamic kpp;
@dynamic okato;
@dynamic rec_bik;
@dynamic inn;
@dynamic to_bank;
@dynamic unit;

@end
