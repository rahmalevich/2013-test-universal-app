//
//  UnitDetailsViewController.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "UnitDetailsViewController.h"
#import "DataManager.h"

#define kTitleSubdivision   @"Подразделение"
#define kTitleToNameF       @"Получатель платежа"
#define kTitleCre           @"Номер счета"
#define kTitleKBK           @"КБК"
#define kTitleKPP           @"КПП"
#define kTitleOKATO         @"ОКАТО"
#define kTitleBIK           @"БИК"
#define kTitleINN           @"ИНН"
#define kTitleBank          @"Банк получателя"

@interface UnitDetailsViewController ()

@end

@implementation UnitDetailsViewController
@synthesize unit = _unit;

#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self refreshUnitDetails];
}

#pragma mark - private methods
- (void)updateUserInterface
{
    [self setupAttributedLabel:_lblSubdivision withTitle:kTitleSubdivision value:_unit.name afterLabel:nil];
    [self setupAttributedLabel:_lblReciever withTitle:kTitleToNameF value:_unit.details.to_name_f afterLabel:_lblSubdivision];
    [self setupAttributedLabel:_lblBank withTitle:kTitleBank value:_unit.details.to_bank afterLabel:_lblReciever];
    [self setupAttributedLabel:_lblBIK withTitle:kTitleBIK value:_unit.details.rec_bik afterLabel:_lblBank];
    [self setupAttributedLabel:_lblNumber withTitle:kTitleCre value:_unit.details.cre afterLabel:_lblBIK];
    [self setupAttributedLabel:_lblKBK withTitle:kTitleKBK value:_unit.details.kbk afterLabel:_lblNumber];
    [self setupAttributedLabel:_lblKPP withTitle:kTitleKPP value:_unit.details.kpp afterLabel:_lblKBK];
    [self setupAttributedLabel:_lblOKATO withTitle:kTitleOKATO value:_unit.details.okato afterLabel:_lblKPP];
    [self setupAttributedLabel:_lblINN withTitle:kTitleINN value:_unit.details.inn afterLabel:_lblOKATO];
    
    [_svContent setContentSize:CGSizeMake(0, _lblINN.frame.origin.y + _lblINN.frame.size.height)];
}

- (void)setupAttributedLabel:(UILabel *)targetLabel withTitle:(NSString *)title value:(NSString *)value afterLabel:(UILabel *)prevLabel
{
    // we adjust text attributes
    float fontSize = targetLabel.font.pointSize;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: %@", title, value]];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:fontSize] range:NSRangeFromString([NSString stringWithFormat:@"0,%d", title.length+1])];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:NSRangeFromString([NSString stringWithFormat:@"%d,%d", title.length+1, attributedString.length - (title.length+1)])];
    targetLabel.attributedText = attributedString;
    
    // then calculate number of lines and frame
    float offset = 10.0f;
    float lineHeight = targetLabel.font.lineHeight;
    float lineWidth = self.view.frame.size.width - 2*offset;
    int numberOfLines = (int)ceilf(attributedString.size.width / lineWidth);
    targetLabel.numberOfLines = numberOfLines;
    targetLabel.frame = CGRectMake(offset, prevLabel ? prevLabel.frame.origin.y + prevLabel.frame.size.height + offset : offset, lineWidth, lineHeight * numberOfLines);
}

- (void)refreshUnitDetails
{
    if ([[DataManager sharedInstance] checkReachability]) {
        [self showBusyAlertWithText:@"Загрузка информации о подразделении..."];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unitDetailsRequestSucceed:) name:kNotificationUnitDetailsRequestSucceed object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unitDetailsRequestFailed:) name:kNotificationUnitDetailsRequestFailed object:nil];
        [[DataManager sharedInstance] getDetailsForUnit:_unit];
    } else {
        [self showConnectionErrorAlert];
    }
}

- (void)unitDetailsRequestSucceed:(NSNotification *)notification
{
    [self dismissBusyAlert];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self updateUserInterface];
}

- (void)unitDetailsRequestFailed:(NSNotification *)notification
{
    [self dismissBusyAlert];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self updateUserInterface];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"При загрузке информации о подразделении произошла ошибка, повторите позднее." delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - actions
- (IBAction)actionRefresh:(UIBarButtonItem *)sender
{
    [self refreshUnitDetails];
}

@end
