//
//  UnitsViewController.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "AbstractViewController.h"

@interface UnitsViewController : AbstractViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate> {
@private
    NSFetchedResultsController *_unitsController;
}

@property (nonatomic, strong) Region *region;
@property (weak, nonatomic) IBOutlet UITableView *tvUnits;

- (IBAction)actionRefresh:(UIBarButtonItem *)sender;

@end
