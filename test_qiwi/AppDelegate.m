//
//  AppDelegate.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 27.05.13.
//
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [MagicalRecord setupCoreDataStack];
    return YES;
}

@end
