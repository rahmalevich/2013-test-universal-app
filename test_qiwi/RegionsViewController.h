//
//  RegionsViewController.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 02.06.13.
//
//

#import "AbstractViewController.h"

@interface RegionsViewController : AbstractViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate> {
@private
    NSFetchedResultsController *_regionsController;
}

@property (weak, nonatomic) IBOutlet UITableView *tvRegions;

- (IBAction)actionRefresh:(UIBarButtonItem *)sender;

@end
