//
//  RegionsViewController.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 02.06.13.
//
//

#import "RegionsViewController.h"
#import "UnitsViewController.h"
#import "IPadMenuViewController.h"
#import "DataManager.h"

@interface RegionsViewController ()

@end

@implementation RegionsViewController

#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refreshRegions];
    _regionsController = [Region MR_fetchAllSortedBy:@"name" ascending:YES withPredicate:nil groupBy:@"firstLetter" delegate:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"UnitsSegue"]) {
        UnitsViewController *destinationController = (UnitsViewController *)segue.destinationViewController;
        [destinationController setRegion:(Region *)sender];
    }
}

#pragma mark - table view delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _regionsController.sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [(id<NSFetchedResultsSectionInfo>)[_regionsController.sections objectAtIndex:section] name];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
//    return _regionsController.sectionIndexTitles;
    
    // it fixes encoding issues
    NSMutableArray *sectionIndexes = [NSMutableArray array];
    for (id<NSFetchedResultsSectionInfo> sectionInfo in _regionsController.sections) {
        [sectionIndexes addObject:[sectionInfo.name substringToIndex:1]];
    }
    return sectionIndexes;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(id<NSFetchedResultsSectionInfo>)[_regionsController.sections objectAtIndex:section] objects].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseId = @"regionsTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
    }
    
    Region *targetRegion = [_regionsController objectAtIndexPath:indexPath];
    cell.textLabel.text = targetRegion.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Region *region = [_regionsController objectAtIndexPath:indexPath];
    
    if ([self.parentViewController isKindOfClass:[IPadMenuViewController class]]) {
        [(IPadMenuViewController *)self.parentViewController selectRegion:region];
    } else {
        [self performSegueWithIdentifier:@"UnitsSegue" sender:region];
    }
}

#pragma mark - fetched results controller delegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tvRegions reloadData];
}

#pragma mark - private methods
- (void)refreshRegions
{
    if ([[DataManager sharedInstance] checkReachability]) {
        [self showBusyAlertWithText:@"Загрузка регионов..."];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(regionsRequestSucceed:) name:kNotificationRegionsRequestSucceed object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(regionsRequestFailed:) name:kNotificationRegionsRequestFailed object:nil];
        [[DataManager sharedInstance] getRegions];
    } else {
        [self showConnectionErrorAlert];
    }
}

- (void)regionsRequestSucceed:(NSNotification *)notification
{
    [self dismissBusyAlert];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)regionsRequestFailed:(NSNotification *)notification
{
    [self dismissBusyAlert];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_tvRegions reloadData];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"При загрузке регионов произошла ошибка, повторите позднее." delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - actions
- (IBAction)actionRefresh:(UIBarButtonItem *)sender
{
    [self refreshRegions];
}
@end
