//
//  UnitDetails.h
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Unit;

@interface UnitDetails : NSManagedObject

@property (nonatomic, retain) NSString * to_name_f;
@property (nonatomic, retain) NSString * cre;
@property (nonatomic, retain) NSString * kbk;
@property (nonatomic, retain) NSString * kpp;
@property (nonatomic, retain) NSString * okato;
@property (nonatomic, retain) NSString * rec_bik;
@property (nonatomic, retain) NSString * inn;
@property (nonatomic, retain) NSString * to_bank;
@property (nonatomic, retain) Unit *unit;

@end
