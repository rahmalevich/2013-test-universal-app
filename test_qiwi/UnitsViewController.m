//
//  UnitsViewController.m
//  test_qiwi
//
//  Created by Mikhail Rahmalevich on 03.06.13.
//
//

#import "UnitsViewController.h"
#import "UnitDetailsViewController.h"
#import "DataManager.h"

@interface UnitsViewController ()

@end

@implementation UnitsViewController
@synthesize region = _region;

#pragma mark - view lifecycle
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"UnitDetailsSegue"]) {
        UnitDetailsViewController *destinationController = (UnitDetailsViewController *)segue.destinationViewController;
        destinationController.unit = (Unit *)sender;
    }
}

#pragma mark - custom setter
- (void)setRegion:(Region *)region
{
    _region = region;
    _unitsController = [Unit MR_fetchAllGroupedBy:@"unitType" withPredicate:[NSPredicate predicateWithFormat:@"region = %@", _region] sortedBy:@"unitType,name" ascending:YES delegate:self];
    [self refreshUnits];
}

#pragma mark - fetched results controller delegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if ([[DataManager sharedInstance] isUnitsRequestComplete]) {
        NSArray *units = controller.fetchedObjects;
        for (Unit *unit in units){
            NSAssert(unit.region != nil, @"REGION IS NULL");
        }
        [_tvUnits reloadData];
    }
}

#pragma mark - table view delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _unitsController.sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *unitType = [(id<NSFetchedResultsSectionInfo>)[_unitsController.sections objectAtIndex:section] name];
    return [self descriptionForType:unitType];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [(id<NSFetchedResultsSectionInfo>)[_unitsController.sections objectAtIndex:section] objects].count;
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseId = @"unitsTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
    }
    
    Unit *targetUnit = [_unitsController objectAtIndexPath:indexPath];
    cell.textLabel.text = targetUnit.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Unit *unit = [_unitsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"UnitDetailsSegue" sender:unit];
}

#pragma mark - private methods
- (void)refreshUnits
{
    if (!_region)
        return;
    
    if ([[DataManager sharedInstance] checkReachability]) {
        [self showBusyAlertWithText:@"Загрузка подразделений..."];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unitsRequestSucceed:) name:kNotificationUnitsRequestSucceed object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unitsRequestFailed:) name:kNotificationUnitsRequestFailed object:nil];
        [[DataManager sharedInstance] getUnitsForRegion:_region];
    } else {
        [self showConnectionErrorAlert];
    }
}

- (void)unitsRequestSucceed:(NSNotification *)notification
{
    [self dismissBusyAlert];
    [[NSNotificationCenter defaultCenter] removeObserver:self];    
}

- (void)unitsRequestFailed:(NSNotification *)notification
{
    [self dismissBusyAlert];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
 
    [_tvUnits reloadData];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"При загрузке подразделений произошла ошибка, повторите позднее." delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil];
    [alertView show];
}

- (NSString *)descriptionForType:(NSString *)type
{
    NSString *result = nil;
    if ([type isEqualToString:@"exm"]) {
        result = @"Экзаменационное подразделение ГИБДД";
    } else if ([type isEqualToString:@"reg"]) {
        result = @"Регистрационное подразделение ГИБДД";
    } else if ([type isEqualToString:@"iaz"]) {
        result = @"Штрафы ГИБДД";
    } else if ([type isEqualToString:@"gto"]) {
        result = @"Пункт государственного технического осмотра";
    }
    return result;
}

#pragma mark - actions
- (IBAction)actionRefresh:(UIBarButtonItem *)sender
{
    [self refreshUnits];
}

@end
